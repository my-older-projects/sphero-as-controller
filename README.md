# sphero-as-controller

This nodejs application provides the possibility to use your Sphero, BB-8 or Ollie as a controller
on Linux OS.

As the body of Sphero and BB-8 is constantly changing (eg: always same pitch and roll values), you need a way to fix you Sphero or BB-8. I do own an Ollie and as I can grab the body, there is no such problem.

Maybe fixing BB-8's head with your hand helps.

(? Or you can use it while it is docked? If there is a magnet that is keeping Sphero's body down, it should work)

Nevertheless this app is working fine with Ollie.

Prerequisite
------------
This application is only compatible with Linux OS with the **uinput** kernel module installed.

Installation
------------
```
npm install 

Usage
-----
Just run ```sudo node .```

Features
--------
There are two modes,

* wasd

Pitch controls w and s, roll controls a and d, yaw controls space and shift. (Car mode)

* plane 

Pitch controls space and shift, roll controls a and d, yaw controls w and s. (Aircraft mode)