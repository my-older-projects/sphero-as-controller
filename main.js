'use strict'; //eslint-disable-line

let Ollie = require('sphero');
let Gamepad = require('virtual-input');
let events = require('events');

module.exports = class {
	constructor(mode) {
		this.bots = {};
		this.pads = {};
		this.fwr = 'space';
		this.bc = 'shift';

		if (mode === 'wasd') {
			this.fwr = 's';
			this.bc = 'w';

			this._2fwr = 'space';
			this._2bc = 'shift';
		} else if (mode === 'plane') {
			this.plane = true;
		}

		process.on('SIGINT', () => {
			this.stop();
		});
	}

	start(uuid) {
		let ollie = this.bots[uuid] = new Ollie(uuid);
		let pad = this.pads[uuid] = new Gamepad();

		this.lastUuid = uuid;

		ollie.connect(() => {
			pad.connect(() => {
				ollie.streamImuAngles(50);
				let emitter = new events.EventEmitter();
				let old = null;

				const g = str => {
					switch (str) {
						case 'space':
							str = 57;
							break;
						case 'shift':
							str = 42;
							break;
						case 'a':
							str = 30;
							break;
						case 'd':
							str = 32;
							break;
						case 'w':
							str = 17;
							break;
						case 's':
							str = 31;
							break;
						default:
							console.error('fuck you', str);
							break;
					}
					return str;
				};

				emitter.on('pad', evs => {
					if (old) {
						if (old.alti === evs.alti) {

						} else if (evs.alti) {
							old.alti && pad.sendEvent({type: 'EV_KEY', code: g(old.alti), value: 0});
							pad.sendEvent({type: 'EV_KEY', code: g(evs.alti), value: 1});
						} else {
							pad.sendEvent({type: 'EV_KEY', code: g(old.alti), value: 0});
						}

						if (old.lr === evs.lr) {

						} else if (evs.lr) {
							old.lr && pad.sendEvent({type: 'EV_KEY', code: g(old.lr), value: 0});
							pad.sendEvent({type: 'EV_KEY', code: g(evs.lr), value: 1});
						} else {
							pad.sendEvent({type: 'EV_KEY', code: g(old.lr), value: 0});
						}

						if (this._2fwr || this.plane) {
							if (old._opt === evs._opt) {

							} else if (evs._opt) {
								old._opt && pad.sendEvent({type: 'EV_KEY', code: g(old._opt), value: 0});
								pad.sendEvent({type: 'EV_KEY', code: g(evs._opt), value: 1});
							} else {
								pad.sendEvent({type: 'EV_KEY', code: g(old._opt), value: 0});
							}
						}
					} else {
						if (evs.alti) {
							pad.sendEvent({type: 'EV_KEY', code: g(evs.alti), value: 1});
						}

						if (evs.lr) {
							pad.sendEvent({type: 'EV_KEY', code: g(evs.lr), value: 1});
						}

						if (this._2fwr) {
							if (evs._opt) {
								pad.sendEvent({type: 'EV_KEY', code: g(evs._opt), value: 1});
							}
						}
					}
					old = evs;
				});

				ollie.on('imuAngles', data => {
					let evs = {};

					if (data.pitchAngle.value[0] > -20 && data.pitchAngle.value[0] < 20) {
						evs.alti = null;
					} else if (data.pitchAngle.value[0] > 21) {
						evs.alti = this.fwr;
					} else {
						evs.alti = this.bc;
					}

					if (data.rollAngle.value[0] > -13 && data.rollAngle.value[0] < 13) {
						evs.lr = null;
					} else if (data.rollAngle.value[0] > 14) {
						evs.lr = 'd';
					} else {
						evs.lr = 'a';
					}

					if (this._2fwr) {
						if (data.yawAngle.value[0] > -20 && data.yawAngle.value[0] < 20) {
							evs._opt = null;
						} else if (data.yawAngle.value[0] > 21) {
							evs._opt = this._2bc;
						} else {
							evs._opt = this._2fwr;
						}
					}

					if (this.plane) {
						if (data.yawAngle.value[0] > -30 && data.yawAngle.value[0] < 30) {
							evs._opt = 'w';
						} else {
							evs._opt = 's';
						}
					}

					emitter.emit('pad', evs);
				});
			});
		});
	}

	stop(uuid, cb) {
		if (typeof uuid !== 'string') {
			cb = uuid;
			uuid = this.lastUuid;
		}

		this.pads[uuid].disconnect(() => {
			this.bots[uuid].sleep(0, 0, 0, () => {
				delete this.bots[uuid];
				delete this.pads[uuid];

				cb && cb();
			});
		});
	}
};
